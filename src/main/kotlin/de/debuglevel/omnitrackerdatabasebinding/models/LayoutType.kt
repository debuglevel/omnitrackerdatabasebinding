package de.debuglevel.omnitrackerdatabasebinding.models

enum class LayoutType(val id: Int) {
    Type0(0),
    Type2(2),
    Type3(3),
    Type4(4),
    Type5(5),
}
