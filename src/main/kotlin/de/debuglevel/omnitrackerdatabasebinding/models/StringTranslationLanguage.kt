package de.debuglevel.omnitrackerdatabasebinding.models

enum class StringTranslationLanguage(val languageCode: String) {
    German("de"),
    English("en")
}
