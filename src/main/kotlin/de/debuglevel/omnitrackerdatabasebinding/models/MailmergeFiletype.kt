package de.debuglevel.omnitrackerdatabasebinding.models

enum class MailmergeFiletype(val id: Int) {
    Word(0),
    PDF(1),
    Type16(16),
    Type31(31),
    Type53(53),
    Type160(160),
}
