package de.debuglevel.omnitrackerdatabasebinding.models

enum class FieldSubtype(val id: Int) {
    probably_Phonenumber(1),
    probably_Email(3),
}
