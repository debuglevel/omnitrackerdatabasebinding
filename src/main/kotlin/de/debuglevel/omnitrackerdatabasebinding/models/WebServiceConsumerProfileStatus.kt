package de.debuglevel.omnitrackerdatabasebinding.models

enum class WebServiceConsumerProfileStatus(
    val id: Int
) {
    NotSet(0),
    Valid(1),
}
